/**
 * Show functions to be exported from the design doc.
 */

var templates = require('duality/templates'),
	types = require('./types'),
	Form = require('couchtypes/forms').Form;


exports.not_found = function (doc, req) {
    return {
        code: 404,
        title: 'Not found',
        content: templates.render('404.html', req, {})
    };
};

exports.new_id = function (doc, req) {
	var form = new Form(types.identity);
	return {
		code: 200,
		title: 'New Identity',
		content: templates.render('identity_create.html', req, {
			form_title: 'New Identity',
			method: 'POST',
			action: '../_update/new_id',
			fields: [],
			button: 'Create',
			ng_controller: 'identityCtrl'
		})
	};
};
