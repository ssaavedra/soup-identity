
var utils = require('duality/utils');


if(utils.isBrowser()) {

  exports.app = angular.module('identityApp', ['CornerCouch']);

  exports.ctrlIdentity = function($scope, $filter, cornercouch) {
    $scope.server = cornercouch(window._baseURL, 'GET');
    $scope.server.session();
    $scope.identitydb = $scope.server.getDB('identity');
    $scope.newentry = $scope.identitydb.newDoc();
    $scope.identitydb.query("app", "utc_only", { include_docs: true, descending: true, limit: 8});

    $scope.submitLogin = function() {
      $scope.server.login($scope.loginUser, $scope.loginPass)
        .success( function() {
          $scope.loginPass = $scope.loginUser = '';
          $scope.showInfo = true;
          $scope.server.getInfo();
          $scope.server.getDatabases();
          $scope.server.getUUIDs(3);
          $scope.server.getUserDoc();
          $scope.identitydb.getInfo();
        });
    };

    function setError(data, status) {
      $scope.errordata = {"status": status, "data": data};
    }

    $scope.rowClick = function(idx) {
      $scope.detail = $scope.identitydb.getQueryDoc(idx);
      $scope.formDetail.$setPristine();
    };

    $scope.nextClick = function() { $scope.identitydb.queryNext(); delete $scope.detail; };
    $scope.prevClick = function() { $scope.identitydb.queryPrev(); delete $scope.detail; };
    $scope.moreClick = function() { $scope.identitydb.queryMore(); };

    $scope.removeClick = function() {
      $scope.detail.remove()
        .success(function() {
          delete $scope.detail;
          $scope.identitydb.queryRefresh();
        });
    };


  };


}
