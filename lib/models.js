
var utils = require('duality/utils');

if(typeof exports === "undefined") {
	var exports = exports || {};
}

if(utils.isBrowser()) {

	exports.identity = Backbone.Model.extend({
		defaults: function() {
			return {
				bits: 512,
				nick: 'unnamed',
				pubkey: '',
				privkey: '',
				privkeyu: '',
				pwprivkey: '',
				distpriv: false
			};
		},

		initialize: function() {
			if(!this.get("bits"))
				this.set({"bits": this.defaults().bits});
			if(!this.get("nick"))
				this.set({"nick": this.defaults().nick});
		},

		generate: function() {
			console.log('Not yet implemented.');
		}
	});

	exports.identityList = Backbone.Collection.extend({

		model: exports.identity,

	});


}

return exports;
