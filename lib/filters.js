/**
 * Filter functions to be exported from the design doc.
 */

exports.by_collection = function (doc, req) {
	if (doc.collection && req.query && req.query.collection && doc.collection == req.query.collection) // Collection matches
		return true;
	else if (req.query && req.query.collection && doc._deleted) // doc deleted
		return true;
	else
		return false;
};

exports.identities = function (doc, req) {
	if (doc.collection == 'identities' || doc._deleted)
		return true;
	else
		return false;
};
