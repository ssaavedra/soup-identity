/**
 * Rewrite settings to be exported from the design doc
 */

module.exports = [

    { "description": "Access to this database" , "from": "_db" , "to"  : "../.." },
    { "from": "_db/*" , "to"  : "../../*" },
    { "description": "Access to this design document" , "from": "_ddoc" , "to"  : "" },
    { "from": "_ddoc/*" , "to"  : "*"},
    { "description": "Access to the main CouchDB API", "from": "_couchdb" , "to"  : "../../.."},
    { "from": "_couchdb/*" , "to"  : "../../../*"},

    {from: '/static/*', to: 'static/*'},
    {from: '/jam/*', to: 'jam/*'},
    {from: '/_help', to: '_list/help_page/pages', query: {
        limit: '2',
        keys: ['sidebar'],
        include_docs: 'true'
    }},

    {
        "from": "/",
        "to": "_list/identity/identities"
    },
    {
        "from": "/new_id",
        "to": "_show/new_id"
    },

    /*

    {from: '/_edit/:page', to: '_list/edit_page/pages', query: {
        limit: '2',
        keys: [':page', 'sidebar'],
        include_docs: 'true'
    }},
    {from: '/_history/:page', to: '_list/history_page/pages', query: {
        limit: '2',
        keys: [':page', 'sidebar'],
        include_docs: 'true'
    }},
    {from: '/_history/:page/:change', to: '_list/history_page/pages', query: {
        limit: '2',
        keys: [':page', 'sidebar'],
        include_docs: 'true'
    }},
    {from: '/_discuss/:page', to: '_list/discussion/discussion', query: {
        startkey: [':page'],
        endkey: [':page', {}],
        include_docs: 'true'
    }},
    {from: '/', to: '_list/page/pages', query: {
        limit: '2',
        page: 'index',
        keys: ['index', 'sidebar'],
        include_docs: 'true'
    }},
    {from: '/test/:page', to: '_view/pages', query: {
        keys: ['index', 'sidebar']
    }},
    {from: '/:page', to: '_list/page/pages', query: {
        limit: '2',
        keys: [':page', 'sidebar'],
        include_docs: 'true'
    }},
    */
    {from: '/kanso-topbar/*', to: 'kanso-topbar/*'},
    {from: '*', to: '_show/not_found'}
];
